FROM ubuntu:22.04

# Set the desired Helm version
ENV HELM_VERSION=v3.15.2

# Install necessary packages and Helm in one layer
RUN apt-get update -y && apt-get install -y --no-install-recommends \
    curl \
    ca-certificates \
    bash \
    git \
    && curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar zxv -C /tmp \
    && mv /tmp/linux-amd64/helm /usr/bin/helm \
    && chmod +x /usr/bin/helm \
    && rm -rf /tmp/linux-amd64 /var/lib/apt/lists/*

# Set the working directory
WORKDIR /apps

# Create a non-root user and group
RUN groupadd -r appuser && useradd -r -g appuser -d /apps -s /sbin/nologin appuser

# Change ownership of the /apps directory
RUN chown -R appuser:appuser /apps

# Switch to the non-root user
USER appuser

# Install Helm cm-push plugins 
RUN helm plugin install https://github.com/chartmuseum/helm-push

# Default to bash
CMD ["bash"]
